package de.yoshiswelt.heron;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.ConnectivityManager;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fathzer.soft.javaluator.DoubleEvaluator;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.Socket;

import static de.yoshiswelt.heron.R.string.ergebnis;

public class Main extends AppCompatActivity {

  EditText wurzel, nachkommastellen;
  TextView ergebnis1, ergebnis2;
  Button ausrechnen;

  private Boolean ergebnisVisible = false;

  private static Boolean ready = false;

  private static BigDecimal result;

  private static String ip = "wetter.codes";
  private static Integer port = 5001;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    wurzel = findViewById(R.id.wurzel);

    nachkommastellen = findViewById(R.id.nachkommastellen);

    ergebnis1 = findViewById(R.id.ergebnis1);

    ergebnis2 = findViewById(R.id.ergebnis2);
    ergebnis2.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (view.equals(ergebnis2)) {
          if (ergebnisVisible) {
            ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData clipData = ClipData.newPlainText("Wurzel von " + wurzel.getText().toString(), ergebnis2.getText().toString());
            if (clipboardManager != null) {
              clipboardManager.setPrimaryClip(clipData);
            }
          }
        }
      }
    });

    ausrechnen = findViewById(R.id.ausrechnen);
    ausrechnen.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (view.equals(ausrechnen)) {
          final String wurzelString = wurzel.getText().toString();
          final String nachkommastellenString = nachkommastellen.getText().toString();

          if (wurzelString.equalsIgnoreCase("") || nachkommastellenString.equalsIgnoreCase("")) {
            Toast.makeText(Main.this, R.string.feld_leer, Toast.LENGTH_LONG).show();
            return;
          }

          if (wurzelString.length() > 15) {
            Toast.makeText(Main.this, R.string.wurzel_zu_lang, Toast.LENGTH_LONG).show();
            return;
          }

          if (!isValid(wurzelString)) {
            Toast.makeText(Main.this, R.string.wurzel_falsch, Toast.LENGTH_LONG).show();
            return;
          }

          final BigDecimal wurzelZahl;

          try {
            wurzelZahl = new BigDecimal(new DoubleEvaluator().evaluate(wurzelString));
          } catch (Exception ex) {
            Toast.makeText(Main.this, R.string.wurzel_falsch, Toast.LENGTH_LONG).show();
            return;
          }

          if (wurzelZahl.toPlainString().startsWith("-")) {
            Toast.makeText(Main.this, R.string.wurzel_falsch, Toast.LENGTH_LONG).show();
            return;
          }

          final int nachkommastellenZahl = Integer.valueOf(nachkommastellenString);

          if (nachkommastellenZahl < 1 || nachkommastellenZahl > 1000) {
            Toast.makeText(Main.this, R.string.nachkomma_falsch, Toast.LENGTH_LONG).show();
            return;
          }

          final BigDecimal epsilon = new BigDecimal(nachkommasToEpsilon(nachkommastellenZahl));

          final Long millis = System.currentTimeMillis();

          if (hasInternet()) {
            if (hasFastInternet()) {
              new Thread() {
                public void run() {
                  result = serverWurzel(wurzelZahl, epsilon);
                  if (result == null)
                    result = localWurzel(wurzelZahl, epsilon);
                  ready = true;
                }
              }.start();
            } else {
              result = localWurzel(wurzelZahl, epsilon);
              ready = true;
            }
          } else {
            result = localWurzel(wurzelZahl, epsilon);
            ready = true;
          }

          while (!ready) {
            try {
              Thread.sleep(1);
            } catch (InterruptedException ex) {
              ex.printStackTrace();
            }
          }
          ready = false;

          final Long newMillis = System.currentTimeMillis();

          final String ergebnisString = result.toPlainString();

          final String wurzelText = getResources().getString(R.string.wurzel_msg).replace("#wurzel#", wurzelZahl.toPlainString());
          final String nachkommaText = getResources().getString(R.string.nachkomma_msg).replace("#nachkomma#", String.valueOf(nachkommastellenZahl));
          final String zeitText = getResources().getString(R.string.zeit).replace("#millis#", String.valueOf(newMillis - millis));
          final String ergebnisText = isWurzelRational(ergebnisString) ? withoutNulls(ergebnisString) : withoutNulls(ergebnisString.substring(0, ergebnisString.indexOf(".") + nachkommastellenZahl + 1));

          ergebnis1.setText(String.format(getResources().getString(ergebnis), wurzelText, nachkommaText, zeitText));
          ergebnis2.setText(ergebnisText);

          if (!ergebnisVisible)
            ergebnisVisible = true;
        }
      }
    });
  }

  private Boolean isValid(String wurzelString) {
    if (wurzelString.startsWith("+") || wurzelString.startsWith("-") || wurzelString.startsWith("*") || wurzelString.startsWith("/") || wurzelString.startsWith(".") || wurzelString.endsWith("+") || wurzelString.endsWith("-") || wurzelString.endsWith("*") || wurzelString.endsWith("/") || wurzelString.endsWith("."))
      return false;

    Character stack = wurzelString.charAt(0);
    for (Character c : wurzelString.substring(1).toCharArray()) {
      if (c.equals('+') || c.equals('-') || c.equals('*') || c.equals('/') || c.equals('.')) {
        if (stack.equals('+') || stack.equals('-') || stack.equals('*') || stack.equals('/') || stack.equals('.'))
          return false;
      }
    }

    return true;
  }

  private BigDecimal serverWurzel(BigDecimal wurzelZahl, BigDecimal epsilon) {
    try {
      Socket socket = new Socket(InetAddress.getByName(ip).getHostAddress(), port);

      DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
      dos.writeUTF(wurzelZahl.toPlainString() + ":" + epsilon.toPlainString());

      DataInputStream dis = new DataInputStream(socket.getInputStream());
      return new BigDecimal(dis.readUTF());
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return null;
  }

  private BigDecimal localWurzel(BigDecimal wurzelZahl, BigDecimal epsilon) {
    BigDecimal x = new BigDecimal(Math.sqrt(Double.valueOf(wurzelZahl.toPlainString())));
    while (x.subtract(y(wurzelZahl, x)).abs().compareTo(epsilon) > 0)
      x = (x.add(y(wurzelZahl, x))).divide(new BigDecimal(2), 10000, RoundingMode.HALF_UP);
    return x;
  }

  private Boolean isWurzelRational(String ergebnisString) {
    return !withoutNulls(ergebnisString).contains(".");
  }

  private String nachkommasToEpsilon(int nachkommaZahl) {
    StringBuilder epsilon = new StringBuilder("1");
    for (int i = 0; i < nachkommaZahl - 1; i++)
      epsilon.insert(0, "0");
    return "0." + epsilon;
  }

  private BigDecimal y(BigDecimal wurzelZahl, BigDecimal x) {
    return wurzelZahl.divide(x, 10000, RoundingMode.HALF_UP);
  }

  private String withoutNulls(String number) {
    boolean rational = true;
    for (int index = number.indexOf(".") + 1; index < number.length(); index++) {
      if (number.charAt(index) != '0') {
        rational = false;
        break;
      }
    }
    return rational ? number.substring(0, number.indexOf(".")) : number;
  }

  private boolean hasInternet() {
    final ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    if (connectivityManager == null) {
      return false;
    }
    return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
  }

  private boolean hasFastInternet() {
    if (hasWifi())
      return true;

    final TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
    if (telephonyManager == null) {
      return false;
    }
    return telephonyManager.getNetworkType() != TelephonyManager.NETWORK_TYPE_GPRS && telephonyManager.getNetworkType() != TelephonyManager.NETWORK_TYPE_EDGE;
  }

  private boolean hasWifi() {
    final ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
    if (connectivityManager == null) {
      return false;
    }
    return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
  }
}